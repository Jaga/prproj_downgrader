# Change prproj version number
import os
import sys
import xml.etree.ElementTree as ET
import gzip
import shutil

print('*** ENSURE FILENAME HAS NO SPACES ***')
if len(sys.argv) < 2:
	print('Supply filename (e.g. python downgrade_prproj.py project_name.prproj)')
	exit()
else:
	filename = sys.argv[1]

# Extract file using Keka
os.system('/Applications/Keka.app/Contents/Resources/keka7z e ' + filename)

# Prase XML and edit version number
downgrade_xml = ET.parse(filename.split('.')[0])
root = downgrade_xml.getroot()

for p in root.iter('Project'):
	if 'Version' in p.attrib:
		p.attrib['Version'] = '1'

xml = filename.split('.')[0]
dg_xml = filename.split('.')[0] + '_downgraded'
downgrade_xml.write(dg_xml)
f_in = open(dg_xml)
f_out = gzip.open(filename.split('.')[0] + '_downgraded.prproj', 'wb')
shutil.copyfileobj(f_in, f_out)

print('Cleaning up files...')
os.system('rm ' + xml)
os.system('rm ' + dg_xml)
print('Finished -- downgraded file: ' + filename.split('.')[0] + '_downgraded.gz')
